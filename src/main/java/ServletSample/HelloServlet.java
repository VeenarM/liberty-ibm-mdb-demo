package ServletSample;

import java.io.IOException;
import java.util.Random;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/servlet")
public class HelloServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Hello! How are you today?");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            QueueConnectionFactory factory = InitialContext.doLookup("jms/remoteCF");
            QueueConnection connection = factory.createQueueConnection();
            QueueSession session = connection.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);

            Queue queue = InitialContext.doLookup("jms/jmsRemoteQueue");
            // Send a message to the queue
            TextMessage message = session.createTextMessage("Your Message Here - " +  new Random().nextInt(100));
            MessageProducer producer = session.createProducer(queue);
            producer.send(message);
            producer.close();
            response.getWriter().append("Message Sent = " + message.getText() );
        } catch (NamingException | JMSException e) {
            e.printStackTrace();
            response.getWriter().append(e.getMessage());
        }
    }
}
