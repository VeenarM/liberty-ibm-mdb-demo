package ServletSample;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.transaction.UserTransaction;

@MessageDriven(name = "ProcessMessageMDBBean", mappedName = "jms/jmsRemoteQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/jmsRemoteQueue")
})
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcessMessageMDB implements MessageListener {

    @Resource
    private UserTransaction userTransaction;

    private static final int DEFAULT_WAIT_TIMEOUT = 15;
    @Override
    public void onMessage(Message message) {
        try {
            userTransaction.begin();
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                // Process the received message
                System.out.println(textMessage.getText() + " Waiting " + DEFAULT_WAIT_TIMEOUT + " seconds...");
                TimeUnit.SECONDS.sleep(DEFAULT_WAIT_TIMEOUT);
                System.out.println(textMessage.getText() + " Completed " + DEFAULT_WAIT_TIMEOUT + " seconds of waiting...");
                double dbl = Math.random();
                System.out.println(dbl);
                // if (dbl > 0.1) {
                //     throw new IllegalArgumentException("INVALID TEST");
                // }
                userTransaction.commit();
                System.out.println("commit completed successfully");
            }
        } catch (Throwable e) {
            System.out.println("ANY OTHER EXCEPTION");
            System.out.println(e);
            try {
                userTransaction.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}